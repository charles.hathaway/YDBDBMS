#################################################################
#								#
# Copyright (c) 2019 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
  createdb
  load_fixture names.sql
  load_fixture names.zwr
}

@test "get oids" {
  octo <<OCTO &> output.txt
SELECT t.typlen
FROM pg_catalog.pg_type t, pg_catalog.pg_namespace n
WHERE t.typnamespace=n.oid AND t.typname='name' AND n.nspname='pg_catalog'
OCTO
  [[ "$(grep -c "ERROR" output.txt)" == "0" ]]
}

@test "get relkind" {
  ## TODO: this is actually really hard because we make a reference to a table
  ##  out of scope in the subquery; for now, skip it
  skip
  octo <<OCTO &> output.txt
SELECT t.typname,t.oid
FROM pg_catalog.pg_type t
  JOIN pg_catalog.pg_namespace n
  ON (t.typnamespace = n.oid)
WHERE n.nspname  != 'pg_toast'
  AND (t.typrelid = 0 OR
    (SELECT c.relkind = 'c' FROM pg_catalog.pg_class c WHERE c.oid = t.typrelid))
OCTO
  [[ "$(grep -c "ERROR" output.txt)" == "0" ]]
}

@test "get tables" {
  octo <<OCTO &> output.txt
SELECT
  NULL AS TABLE_CAT,
  n.nspname AS TABLE_SCHEM,
  c.relname AS TABLE_NAME,
  CASE n.nspname ~ '^pg_'
  OR n.nspname = 'information_schema' WHEN true THEN CASE WHEN n.nspname = 'pg_catalog'
  OR n.nspname = 'information_schema' THEN CASE c.relkind WHEN 'r' THEN 'SYSTEM TABLE' WHEN 'v' THEN 'SYSTEM VIEW' WHEN 'i' THEN 'SYSTEM INDEX' ELSE NULL END WHEN n.nspname = 'pg_toast' THEN CASE c.relkind WHEN 'r' THEN 'SYSTEM TOAST TABLE' WHEN 'i' THEN 'SYSTEM TOAST INDEX' ELSE NULL END ELSE CASE c.relkind WHEN 'r' THEN 'TEMPORARY TABLE' WHEN 'p' THEN 'TEMPORARY TABLE' WHEN 'i' THEN 'TEMPORARY INDEX' WHEN 'S' THEN 'TEMPORARY SEQUENCE' WHEN 'v' THEN 'TEMPORARY VIEW' ELSE NULL END END WHEN false THEN CASE c.relkind WHEN 'r' THEN 'TABLE' WHEN 'p' THEN 'TABLE' WHEN 'i' THEN 'INDEX' WHEN 'S' THEN 'SEQUENCE' WHEN 'v' THEN 'VIEW' WHEN 'c' THEN 'TYPE' WHEN 'f' THEN 'FOREIGN TABLE' WHEN 'm' THEN 'MATERIALIZED VIEW' ELSE NULL END ELSE NULL END AS TABLE_TYPE,
  d.description AS REMARKS,
  '' as TYPE_CAT,
  '' as TYPE_SCHEM,
  '' as TYPE_NAME,
  '' AS SELF_REFERENCING_COL_NAME,
  '' AS REF_GENERATION
FROM
  pg_catalog.pg_namespace n,
  pg_catalog.pg_class c
  LEFT JOIN pg_catalog.pg_description d ON (
    c.oid = d.objoid
    AND d.objsubid = 0
  )
  LEFT JOIN pg_catalog.pg_class dc ON (
    d.classoid = dc.oid
    AND dc.relname = 'pg_class'
  )
  LEFT JOIN pg_catalog.pg_namespace dn ON (
    dn.oid = dc.relnamespace
    AND dn.nspname = 'pg_catalog'
  )
WHERE
  c.relnamespace = n.oid
  AND (
    false
    OR (
      c.relkind IN ('r', 'p')
      AND n.nspname ! ~ '^pg_'
      AND n.nspname <> 'information_schema'
    )
    OR (
      c.relkind = 'r'
      AND (
        n.nspname = 'pg_catalog'
        OR n.nspname = 'information_schema'
      )
    )
    OR (
      c.relkind = 'v'
      AND n.nspname <> 'pg_catalog'
      AND n.nspname <> 'information_schema'
    )
  )
ORDER BY
  TABLE_TYPE,
  TABLE_SCHEM,
  TABLE_NAME
OCTO
  [[ "$(grep -c "ERROR" output.txt)" == "0" ]]
}

@test "test1" {
  octo <<OCTO &> output.txt
 SELECT NULL          AS TABLE_CAT,
       n.nspname     AS TABLE_SCHEM,
       c.relname     AS TABLE_NAME,
       CASE n.nspname ~ '^pg_'
             OR n.nspname = 'information_schema'
         WHEN true THEN
           CASE
             WHEN n.nspname = 'pg_catalog'
                   OR n.nspname = 'information_schema' THEN
               CASE c.relkind
                 WHEN 'r' THEN 'SYSTEM TABLE'
                 WHEN 'v' THEN 'SYSTEM VIEW'
                 WHEN 'i' THEN 'SYSTEM INDEX'
                 ELSE NULL
               end
             WHEN n.nspname = 'pg_toast' THEN
               CASE c.relkind
                 WHEN 'r' THEN 'SYSTEM TOAST TABLE'
                 WHEN 'i' THEN 'SYSTEM TOAST INDEX'
                 ELSE NULL
               end
             ELSE
               CASE c.relkind
                 WHEN 'r' THEN 'TEMPORARY TABLE'
                 WHEN 'p' THEN 'TEMPORARY TABLE'
                 WHEN 'i' THEN 'TEMPORARY INDEX'
                 WHEN 'S' THEN 'TEMPORARY SEQUENCE'
                 WHEN 'v' THEN 'TEMPORARY VIEW'
                 ELSE NULL
               end
           end
         WHEN false THEN
           CASE c.relkind
             WHEN 'r' THEN 'TABLE'
             WHEN 'p' THEN 'TABLE'
             WHEN 'i' THEN 'INDEX'
             WHEN 'S' THEN 'SEQUENCE'
             WHEN 'v' THEN 'VIEW'
             WHEN 'c' THEN 'TYPE'
             WHEN 'f' THEN 'FOREIGN TABLE'
             WHEN 'm' THEN 'MATERIALIZED VIEW'
             ELSE NULL
           end
         ELSE NULL
       end           AS TABLE_TYPE,
       d.description AS REMARKS,
       ''            AS TYPE_CAT,
       ''            AS TYPE_SCHEM,
       ''            AS TYPE_NAME,
       ''            AS SELF_REFERENCING_COL_NAME,
       ''            AS REF_GENERATION
FROM   pg_catalog.pg_namespace n,
       pg_catalog.pg_class c
       LEFT JOIN pg_catalog.pg_description d
              ON ( c.oid = d.objoid
                   AND d.objsubid = 0 )
       LEFT JOIN pg_catalog.pg_class dc
              ON ( d.classoid = dc.oid
                   AND dc.relname = 'pg_class' )
       LEFT JOIN pg_catalog.pg_namespace dn
              ON ( dn.oid = dc.relnamespace
                   AND dn.nspname = 'pg_catalog' )
WHERE  c.relnamespace = n.oid
       AND c.relname LIKE '%'
       AND ( false
              OR ( c.relkind IN ( 'r', 'p' )
                   AND n.nspname !~ '^pg_'
                   AND n.nspname <> 'information_schema' )
              OR ( c.relkind = 'r'
                   AND ( n.nspname = 'pg_catalog'
                          OR n.nspname = 'information_schema' ) )
              OR ( c.relkind = 'v'
                   AND n.nspname <> 'pg_catalog'
                   AND n.nspname <> 'information_schema' ) )
ORDER  BY table_type,
          table_schem,
          table_name
OCTO
  [[ "$(grep -c "ERROR" output.txt)" == "0" ]]
}

@test "test2" {
  octo <<OCTO &> output.txt
 SELECT NULL          AS PROCEDURE_CAT,
       n.nspname     AS PROCEDURE_SCHEM,
       p.proname     AS PROCEDURE_NAME,
       NULL,
       NULL,
       NULL,
       d.description AS REMARKS,
       2             AS PROCEDURE_TYPE,
       p.proname
       || '_'
       || p.oid      AS SPECIFIC_NAME
FROM   pg_catalog.pg_namespace n,
       pg_catalog.pg_proc p
       left join pg_catalog.pg_description d
              ON ( p.oid = d.objoid )
       left join pg_catalog.pg_class c
              ON ( d.classoid = c.oid
                   AND c.relname = 'pg_proc' )
       left join pg_catalog.pg_namespace pn
              ON ( c.relnamespace = pn.oid
                   AND pn.nspname = 'pg_catalog' )
WHERE  p.pronamespace = n.oid
ORDER  BY procedure_schem,
          procedure_name,
          p.oid :: text
OCTO
  [[ "$(grep -c "ERROR" output.txt)" == "0" ]]
}

@test "test3" {
  skip # This test isn't working just yet
  octo <<OCTO &> output.txt
 SELECT NULL                                         AS type_cat,
       n.nspname                                    AS type_schem,
       t.typname                                    AS type_name,
       NULL                                         AS class_name,
       CASE
         WHEN t.typtype = 'c' THEN 2002
         ELSE 2001
       END                                          AS data_type,
       pg_catalog.Obj_description(t.oid, 'pg_type') AS remarks,
       CASE
         WHEN t.typtype = 'd' THEN (SELECT CASE
                                             WHEN typname = 'date' THEN 91
                                             WHEN typname = 'time[]' THEN 2003
                                             WHEN typname = '_name' THEN 2003
                                             WHEN typname = 'numeric[]' THEN
                                             2003
                                             WHEN typname = 'timestamptz[]' THEN
                                             2003
                                             WHEN typname = 'refcursor' THEN
                                             2012
                                             WHEN typname = 'timetz[]' THEN 2003
                                             WHEN typname = 'money[]' THEN 2003
                                             WHEN typname = 'bit' THEN -7
                                             WHEN typname = 'uuid' THEN 1111
                                             WHEN typname = '_money' THEN 2003
                                             WHEN typname = 'int2' THEN 5
                                             WHEN typname = 'name[]' THEN 2003
                                             WHEN typname = 'int4' THEN 4
                                             WHEN typname = '_varchar' THEN 2003
                                             WHEN typname = 'uuid[]' THEN 2003
                                             WHEN typname = 'xml' THEN 2009
                                             WHEN typname = 'int8' THEN -5
                                             WHEN typname = 'text' THEN 12
                                             WHEN typname = '_numeric' THEN 2003
                                             WHEN typname = '_timestamp' THEN
                                             2003
                                             WHEN typname = 'json[]' THEN 2003
                                             WHEN typname = '_bool' THEN 2003
                                             WHEN typname = 'date[]' THEN 2003
                                             WHEN typname = 'varchar[]' THEN
                                             2003
                                             WHEN typname = '_oid' THEN 2003
                                             WHEN typname = '_json' THEN 2003
                                             WHEN typname = 'int2[]' THEN 2003
                                             WHEN typname = 'int4[]' THEN 2003
                                             WHEN typname = 'bpchar' THEN 1
                                             WHEN typname = 'name' THEN 12
                                             WHEN typname = 'bit[]' THEN 2003
                                             WHEN typname = 'int8[]' THEN 2003
                                             WHEN typname = 'timestamptz' THEN
                                             93
                                             WHEN typname = 'refcursor[]' THEN
                                             2003
                                             WHEN typname = '_bpchar' THEN 2003
                                             WHEN typname = 'bpchar[]' THEN 2003
                                             WHEN typname = 'bytea' THEN -2
                                             WHEN typname = 'bool' THEN -7
                                             WHEN typname = 'bool[]' THEN 2003
                                             WHEN typname = '_refcursor' THEN
                                             2003
                                             WHEN typname = 'numeric' THEN 2
                                             WHEN typname = 'oid' THEN -5
                                             WHEN typname = 'point' THEN 1111
                                             WHEN typname = '_timestamptz' THEN
                                             2003
                                             WHEN typname = '_float4' THEN 2003
                                             WHEN typname = 'bytea[]' THEN 2003
                                             WHEN typname = 'json' THEN 1111
                                             WHEN typname = 'timestamp[]' THEN
                                             2003
                                             WHEN typname = '_char' THEN 2003
                                             WHEN typname = '_time' THEN 2003
                                             WHEN typname = '_float8' THEN 2003
                                             WHEN typname = 'timestamp' THEN 93
                                             WHEN typname = '_bytea' THEN 2003
                                             WHEN typname = 'oid[]' THEN 2003
                                             WHEN typname = '_bit' THEN 2003
                                             WHEN typname = '_int8' THEN 2003
                                             WHEN typname = '_date' THEN 2003
                                             WHEN typname = 'varchar' THEN 12
                                             WHEN typname = '_int4' THEN 2003
                                             WHEN typname = 'float8' THEN 8
                                             WHEN typname = '_point' THEN 2003
                                             WHEN typname = '_uuid' THEN 2003
                                             WHEN typname = 'float4' THEN 7
                                             WHEN typname = 'point[]' THEN 2003
                                             WHEN typname = '_int2' THEN 2003
                                             WHEN typname = '_timetz' THEN 2003
                                             WHEN typname = 'char[]' THEN 2003
                                             WHEN typname = 'float8[]' THEN 2003
                                             WHEN typname = 'money' THEN 8
                                             WHEN typname = '_xml' THEN 2003
                                             WHEN typname = 'float4[]' THEN 2003
                                             WHEN typname = 'text[]' THEN 2003
                                             WHEN typname = 'xml[]' THEN 2003
                                             WHEN typname = 'char' THEN 1
                                             WHEN typname = 'time' THEN 92
                                             WHEN typname = '_text' THEN 2003
                                             WHEN typname = 'timetz' THEN 92
                                             ELSE 1111
                                           END
                                    FROM   pg_type
                                    WHERE  oid = t.typbasetype)
         ELSE NULL
       END                                          AS base_type
FROM   pg_catalog.pg_type t,
       pg_catalog.pg_namespace n
WHERE  t.typnamespace = n.oid
       AND n.nspname != 'pg_catalog'
       AND n.nspname != 'pg_toast'
       AND t.typtype IN ( 'c', 'd' )
ORDER  BY data_type,
          type_schem,
          type_name
OCTO
  [[ "$(grep -c "ERROR" output.txt)" == "0" ]]
}

@test "select database version" {
  octo <<OCTO &> output.txt
SELECT version();
OCTO
  # For now, not crashing is considered acceptable; it seems likely that the version
  # will change often and I don't feel like updating this test all the time
}
